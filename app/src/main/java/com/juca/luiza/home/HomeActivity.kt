package com.juca.luiza.home

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.juca.luiza.R
import com.juca.luiza.login.LoginActivity
import com.juca.luiza.login.SignupActivity
import com.juca.luiza.models.User

class HomeActivity : AppCompatActivity() {

    private lateinit var user: User
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var navView: NavigationView
    private lateinit var actionBarDrawerToggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        navView = drawerLayout.findViewById(R.id.navView)
        initializeDrawer()
        val hView = navView.getHeaderView(0)

        user = User()
        initializeUserView(navView, hView)
    }

    private fun initializeUserView(navigationView: NavigationView, hView: View) {
        if (isUserConnected()) {
            //    fab.setVisibility(View.VISIBLE)
            navigationView.menu.clear()
            navigationView.inflateMenu(R.menu.navigation_login)

            val userEmail = hView.findViewById(R.id.user_email) as TextView
            userEmail.visibility = View.VISIBLE
            userEmail.setText(user.email)
        } else {
            //   fab.setVisibility(View.INVISIBLE)
            navigationView.menu.clear()
            navigationView.inflateMenu(R.menu.navigation_view)
        }
    }

    private fun isUserConnected(): Boolean {
        val user = FirebaseAuth.getInstance().currentUser ?: return false
        setCurrentUser(user)
        return true
    }

    private fun setCurrentUser(firebaseUser: FirebaseUser?): User {
        if (firebaseUser == null) {
            return user
        }
        user.email = firebaseUser.email
        return user
    }


    private fun initializeDrawer() {
        actionBarDrawerToggle = ActionBarDrawerToggle(this, drawerLayout,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        actionBarDrawerToggle.syncState()
        if (navView.menu != null) {
            navView.setNavigationItemSelectedListener(onNavigationItemSelectedListener)
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        actionBarDrawerToggle.onConfigurationChanged(newConfig)
    }

    private val onNavigationItemSelectedListener = NavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.nav_login -> {
                val intent = Intent(this@HomeActivity, LoginActivity::class.java)
                startActivity(intent)
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_signup -> {
                val intent = Intent(this@HomeActivity, SignupActivity::class.java)
                startActivity(intent)
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_logout -> {
                val user = FirebaseAuth.getInstance().currentUser
                if (user != null) {
                    FirebaseAuth.getInstance().signOut()
                    val intent = Intent(this@HomeActivity, HomeActivity::class.java)
                    startActivity(intent)
                    intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
                    finish()
                }
                return@OnNavigationItemSelectedListener true
            }
        }
        navView.setCheckedItem(item.itemId)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START, true)
        }
        false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                drawerLayout.openDrawer(GravityCompat.START)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        const val TAG = "HomeActivity"

        fun start(context: Context) {
            val intent = Intent(context, HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or
                    Intent.FLAG_ACTIVITY_CLEAR_TOP
            context.startActivity(intent)
        }
    }
}