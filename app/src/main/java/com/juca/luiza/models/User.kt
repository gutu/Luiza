package com.juca.luiza.models

class User {

    var id: String? = null
    var username: String? = null
    var name: String? = null
    var firstname: String? = null
    var email: String? = null
    var address: String? = null
    var address_complement: String? = null
    var zip: String? = null
    var town: String? = null
    var phone: String? = null
    var isPrivacy: Boolean = false
    var icon: Int = 0
    var cakeList: List<Cake>? = null

    constructor() {

    }

    constructor(name: String, icon: Int) {
        this.name = name
        this.icon = icon
    }

    constructor(email: String) {
        this.email = email
    }

}
