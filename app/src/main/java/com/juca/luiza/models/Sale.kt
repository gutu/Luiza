package com.juca.luiza.models

class Sale {

    var id: String? = null
    var timestamp: String? = null
    var cakeList: List<Cake>? = null
    var user: User? = null
}
