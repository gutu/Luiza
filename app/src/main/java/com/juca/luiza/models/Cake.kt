package com.juca.luiza.models

class Cake(var name: String?, var photo: Int, var minPrice: Double, var user: User?) {

    var id: String? = null
    var description: String? = null
    var timeTodo: String? = null
    var nbLikes: Int = 0
    private var ingredientList: MutableList<Ingredient>? = null
    var receiptList: List<Receipt>? = null
    private var allergenList: MutableList<Allergen>? = null
    var tag: Tag? = null
    private var themeList: MutableList<Theme>? = null
    private var commentList: MutableList<Comment>? = null

    fun getIngredientList(): List<Ingredient>? {
        return ingredientList
    }

    fun setIngredientList(ingredientList: MutableList<Ingredient>) {
        this.ingredientList = ingredientList
    }

    fun getAllergenList(): List<Allergen>? {
        return allergenList
    }

    fun setAllergenList(allergenList: MutableList<Allergen>) {
        this.allergenList = allergenList
    }

    fun getThemeList(): List<Theme>? {
        return themeList
    }

    fun setThemeList(themeList: MutableList<Theme>) {
        this.themeList = themeList
    }

    fun getCommentList(): List<Comment>? {
        return commentList
    }

    fun setCommentList(commentList: MutableList<Comment>) {
        this.commentList = commentList
    }

    fun addAllergen(allergen: Allergen) {
        allergenList!!.add(allergen)
    }

    fun addIngredient(ingredient: Ingredient) {
        ingredientList!!.add(ingredient)
    }

    fun addComment(comment: Comment) {
        commentList!!.add(comment)
    }

    fun addTheme(theme: Theme) {
        themeList!!.add(theme)
    }
}
