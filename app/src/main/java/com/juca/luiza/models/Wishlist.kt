package com.juca.luiza.models

class Wishlist {

    var id: String? = null
    var cakeList: List<Cake>? = null
    var user: User? = null
}
