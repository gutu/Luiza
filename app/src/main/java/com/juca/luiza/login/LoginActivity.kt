package com.juca.luiza.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import butterknife.ButterKnife
import com.google.firebase.auth.FirebaseAuth
import com.juca.luiza.home.HomeActivity
import com.juca.luiza.R
import kotlinx.android.synthetic.main.activity_login.emailInput
import kotlinx.android.synthetic.main.activity_login.emailLayout
import kotlinx.android.synthetic.main.activity_login.loginAction
import kotlinx.android.synthetic.main.activity_login.passwordInput
import kotlinx.android.synthetic.main.activity_login.passwordLayout
import kotlinx.android.synthetic.main.activity_login.progressBar
import kotlinx.android.synthetic.main.activity_login.signupAction

class LoginActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        ButterKnife.bind(this)

        auth = FirebaseAuth.getInstance()

        loginAction.setOnClickListener { submitForm() }
        signupAction.setOnClickListener {
            val intent = Intent(this@LoginActivity, SignupActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun submitForm() {
        val email = emailInput.text.toString().trim({ it <= ' ' })
        val password = passwordInput.text.toString().trim({ it <= ' ' })

        if (!checkEmail()) {
            return
        }
        if (!checkPassword()) {
            return
        }
        emailLayout.isErrorEnabled = false
        passwordLayout.isErrorEnabled = false

        progressBar.visibility = View.VISIBLE
        auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this@LoginActivity) { task ->
                    progressBar.visibility = View.GONE
                    progressBar.hideKeyboard()
                    if (!task.isSuccessful) {
                        Snackbar.make(emailInput, getString(R.string.auth_failed), Snackbar.LENGTH_LONG).show()
                    } else {
                        val intent = Intent(this@LoginActivity, HomeActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                }
    }

    private fun checkEmail(): Boolean {
        val email = emailInput.text.toString().trim({ it <= ' ' })
        if (email.isEmpty() || !isEmailValid(email)) {

            emailLayout.isErrorEnabled = true
            emailLayout.error = getString(R.string.err_msg_email)
            emailInput.error = getString(R.string.err_msg_required)
            requestFocus(emailInput)
            return false
        }
        emailLayout.isErrorEnabled = false
        return true
    }

    private fun checkPassword(): Boolean {

        val password = passwordInput.text.toString().trim({ it <= ' ' })
        if (password.isEmpty() || !isPasswordValid(password)) {

            passwordLayout.error = getString(R.string.err_msg_password)
            passwordInput.error = getString(R.string.err_msg_required)
            requestFocus(passwordInput)
            return false
        }
        passwordLayout.isErrorEnabled = false
        return true
    }

    private fun isEmailValid(email: String): Boolean {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun isPasswordValid(password: String): Boolean {
        return password.length >= 6
    }

    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

    override fun onResume() {
        super.onResume()
        progressBar.visibility = View.GONE
    }

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, LoginActivity::class.java)
            context.startActivity(intent)
        }
    }
}