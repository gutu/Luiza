package com.juca.luiza.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.FirebaseDatabase
import com.juca.luiza.home.HomeActivity
import com.juca.luiza.R
import com.juca.luiza.models.User
import kotlinx.android.synthetic.main.activity_signup.emailInput
import kotlinx.android.synthetic.main.activity_signup.emailLayout
import kotlinx.android.synthetic.main.activity_signup.loginAction
import kotlinx.android.synthetic.main.activity_signup.passwordInput
import kotlinx.android.synthetic.main.activity_signup.passwordLayout
import kotlinx.android.synthetic.main.activity_signup.progressBar
import kotlinx.android.synthetic.main.activity_signup.signupAction

private const val TAG: String = "SignupActivity"

class SignupActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        auth = FirebaseAuth.getInstance()

        signupAction.setOnClickListener { submitForm() }
        loginAction.setOnClickListener {
            val intent = Intent(this@SignupActivity, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun submitForm() {

        val email = emailInput.text.toString().trim({ it <= ' ' })
        val password = passwordInput.text.toString().trim({ it <= ' ' })

        if (!checkEmail()) {
            return
        }
        if (!checkPassword()) {
            return
        }

        emailLayout.isErrorEnabled = false
        passwordLayout.isErrorEnabled = false

        progressBar.visibility = View.VISIBLE
        //create user
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this@SignupActivity, { task ->
                    Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful)
                    progressBar.visibility = View.GONE
                    progressBar.hideKeyboard()
                    if (!task.isSuccessful) {
                        Snackbar.make(emailInput, "Une erreur est survenue", Snackbar.LENGTH_LONG).show()
                    } else {
                        createNewUser(task.result.user)
                        Snackbar.make(emailInput, "You are successfully Registered !!", Snackbar.LENGTH_LONG).show()
                        val intent = Intent(this@SignupActivity, HomeActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                })
    }

    private fun createNewUser(userFromRegistration: FirebaseUser) {
        val email = userFromRegistration.email.toString()
        val userId = userFromRegistration.uid

        val user = User(email)

        val mDatabase = FirebaseDatabase.getInstance().reference
        mDatabase.child("users").child(userId).setValue(user)
    }

    private fun checkEmail(): Boolean {
        val email = emailInput.text.toString().trim({ it <= ' ' })
        if (email.isEmpty() || !isEmailValid(email)) {

            emailLayout.isErrorEnabled = true
            emailLayout.error = getString(R.string.err_msg_email)
            emailInput.error = getString(R.string.err_msg_required)
            requestFocus(emailInput)
            return false
        }
        emailLayout.isErrorEnabled = false
        return true
    }

    private fun checkPassword(): Boolean {

        val password = passwordInput.text.toString().trim({ it <= ' ' })
        if (password.isEmpty() || !isPasswordValid(password)) {

            passwordLayout.error = getString(R.string.err_msg_password)
            passwordInput.error = getString(R.string.err_msg_required)
            requestFocus(passwordInput)
            return false
        }
        passwordLayout.isErrorEnabled = false
        return true
    }

    private fun isEmailValid(email: String): Boolean {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun isPasswordValid(password: String): Boolean {
        return password.length >= 6
    }

    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun onResume() {
        super.onResume()
        progressBar.visibility = View.GONE
    }
}