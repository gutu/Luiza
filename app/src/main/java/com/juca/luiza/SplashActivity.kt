package com.juca.luiza

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.juca.luiza.home.HomeActivity

class SplashActivity : AppCompatActivity() {

    private var TIME: Long = 5000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        //TODO if first connexion, display onBoarding
        Handler().postDelayed({
            HomeActivity.start(this)
        }, TIME)

    }
}
